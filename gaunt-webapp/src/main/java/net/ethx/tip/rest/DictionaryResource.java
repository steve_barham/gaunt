package net.ethx.tip.rest;

import net.ethx.tip.Constants;
import net.ethx.tip.app.DictionaryManager;
import net.ethx.tip.data.Dictionary;
import net.ethx.tip.util.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.toList;

@Path("dictionary")
public class DictionaryResource {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{dict}")
    public List<String> define(@PathParam("dict") String dictionaryName,
                               @QueryParam("letters") String letters) {
        final Dictionary dictionary = DictionaryManager.instance().getDictionary(dictionaryName);
        checkNotNull(dictionary, "No dictionary found with name %s", dictionaryName);
        checkArgument(letters != null && letters.length() <= Constants.MAX_LETTERS, "Parameter letters must be non-null, <= %s characters", Constants.MAX_LETTERS);

        checkUnknowns(letters, Constants.MAX_UNKNOWN);

        return Timer.time(() -> dictionary.solve(letters).sorted().collect(toList()), log, TimeUnit.MICROSECONDS, "Solving '%s' against '%s'", letters, dictionary.definition().name());
    }

    protected static void checkUnknowns(String letters, int maximum) {
        int unknownCount = 0;
        for (char c : letters.toCharArray()) {
            unknownCount += c == '?' ? 1 : 0;
        }
        checkArgument(unknownCount <= maximum, "Maximum of %s unknowns supported, but found %s in '%s'", maximum, unknownCount, letters);
    }
}

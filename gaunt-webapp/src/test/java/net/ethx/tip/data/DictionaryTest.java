package net.ethx.tip.data;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DictionaryTest {
    private Dictionary dict;

    @Before
    public void setup() throws Exception {
        dict = new PowerSetDictionary(Definition.load("/sowpods-en.json"));
    }

    @Test
    public void solveInvalidChars() throws Exception {
        final Set<String> solutions = dict.solve("be n").collect(Collectors.toSet());
        assertEquals(solutions, new HashSet<>(Arrays.asList("BE", "NE", "EN", "BEN", "NEB")));
    }

    @Test
    public void solve() throws Exception {
        final Set<String> solutions = dict.solve("ben").collect(Collectors.toSet());
        assertEquals(solutions, new HashSet<>(Arrays.asList("BE", "NE", "EN", "BEN", "NEB")));
    }

    @Test
    public void solveUnknowns() throws Exception {
        final Set<String> solutions = dict.solve("be?").collect(Collectors.toSet());
        assertTrue(solutions.containsAll(Arrays.asList("BE", "NE", "EN", "BEN", "NEB")));
    }

    @Test
    public void bulk() {
        System.out.printf("Took %sms to run 1000 iterations", TimeUnit.NANOSECONDS.toMillis(time(() -> {
            int count = 0;
            for (int i = 0; i < 1000; i++) {
                count += dict.solve("nimrod").count();
            }
        })));
    }

    @Test
    public void solveMultipleUnknowns() throws Exception {
        dict.solve("beansta??").collect(Collectors.toSet()).size();
    }

    public static long time(Runnable runnable) {
        final long then = System.nanoTime();
        runnable.run();
        return System.nanoTime() - then;
    }
}
package net.ethx.tip.data;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Multimaps;
import com.google.common.primitives.Ints;
import net.ethx.tip.Constants;
import net.ethx.tip.util.PrimeStream;

import java.math.BigInteger;
import java.text.Normalizer;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static net.ethx.tip.util.MoreStreams.zip;

//  todo: scoring
public class PowerSetDictionary implements Dictionary {
    private final Definition definition;
    private final ImmutableMap<Character, BigInteger> tilePrimes;
    private final int[] powers;
    private final ImmutableListMultimap<BigInteger, String> indexedWords;
    private final String replacePattern;

    public PowerSetDictionary(Definition definition) {
        this.definition = definition;

        //  collect chars, map to primes
        final ImmutableMap.Builder<Character, BigInteger> tilePrimes = ImmutableMap.builder();
        zip(definition.tiles().stream(), PrimeStream.generate().map(BigInteger::valueOf), (c, i) -> new AbstractMap.SimpleImmutableEntry<>(c, i))
                .forEach(e -> tilePrimes.put(e.getKey(), e.getValue()));
        this.tilePrimes = tilePrimes.build();

        //  define powers as required
        this.powers = IntStream.rangeClosed(0, Constants.MAX_LETTERS)
                .map(i -> Ints.checkedCast((long) Math.pow(2, i)))
                .toArray();

        //  create a replacement pattern from the valid tiles
        this.replacePattern = "[^" + definition.tiles().stream().map(Object::toString).collect(Collectors.joining("")) + "]";

        //  index the definition words
        this.indexedWords = Multimaps.index(definition.words(), this::index);
    }

    @Override
    public Definition definition() {
        return definition;
    }

    @Override
    public Stream<String> solve(String in) {
        final int index = in.indexOf('?');
        if (index < 0) {
            final int limit = Ints.checkedCast((long) Math.pow(2, in.length()));
            final char[] chars = explode(in);
            return IntStream.rangeClosed(0, limit)
                    .parallel()
                    .mapToObj(i -> {
                        BigInteger sum = BigInteger.ONE;
                        for (int j = 0; j < chars.length; j++) {
                            if ((i & powers[j]) == powers[j]) {
                                sum = sum.multiply(tilePrimes.get(chars[j]));
                            }
                        }
                        return indexedWords.get(sum);
                    })
                    .filter(l -> !l.isEmpty())
                    .flatMap(Collection::stream)
                    .distinct();
        } else {
            Stream<String> ret = Stream.of();

            final char[] chars = in.toCharArray();
            for (Character character : tilePrimes.keySet()) {
                chars[index] = character;
                ret = Stream.concat(ret, solve(new String(chars)));
            }

            return ret.distinct();
        }

    }

    private BigInteger index(String in) {
        final char[] chars = explode(in);

        BigInteger sum = BigInteger.ONE;
        for (final char c : chars) {
            final BigInteger augend = tilePrimes.get(c);
            if (augend == null) {
                throw new IllegalArgumentException("Unknown character '" + c + "'");
            }
            sum = sum.multiply(augend);
        }
        return sum;
    }

    private char[] explode(String in) {
        return Normalizer.normalize(in, Normalizer.Form.NFD).toUpperCase().replaceAll(replacePattern, "").toCharArray();
    }
}

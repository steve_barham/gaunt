package net.ethx.tip.rest;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class JsonExceptionMapper implements ExceptionMapper<Exception> {
    private static final ObjectMapper mapper = new ObjectMapper();

    @Context
    UriInfo uri;

    @Override
    public Response toResponse(Exception e) {
        return Response.serverError().entity(new ErrorReport(uri.getRequestUri().toString(), e.getMessage())).build();
    }


    @JsonAutoDetect(JsonMethod.NONE)
    public static class ErrorReport {
        @JsonProperty("uri")
        private final String uri;
        @JsonProperty("message")
        private final String message;

        @JsonCreator
        public ErrorReport(@JsonProperty("uri") String uri,
                           @JsonProperty("message") String message) {
            this.uri = uri;
            this.message = message;
        }
    }
}

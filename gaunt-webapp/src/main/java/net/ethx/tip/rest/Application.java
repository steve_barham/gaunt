package net.ethx.tip.rest;

import com.google.common.collect.ImmutableSet;

import javax.ws.rs.ApplicationPath;
import java.util.Set;

@ApplicationPath("/app")
public class Application extends javax.ws.rs.core.Application {
    @Override
    public Set<Class<?>> getClasses() {
        return ImmutableSet.of(DictionaryResource.class);
    }
}

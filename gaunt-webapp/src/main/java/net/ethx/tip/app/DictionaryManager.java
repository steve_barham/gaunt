package net.ethx.tip.app;

import net.ethx.tip.data.Definition;
import net.ethx.tip.data.Dictionary;
import net.ethx.tip.data.PowerSetDictionary;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.uniqueIndex;

public class DictionaryManager {
    public static final DictionaryManager instance = new DictionaryManager();

    public static DictionaryManager instance() {
        return instance;
    }

    private final Map<String, Dictionary> dictionaries;

    private DictionaryManager() {
        final List<Dictionary> ds = Arrays.asList("/sowpods-en.json").stream()
                .map(Definition::load)
                .map(PowerSetDictionary::new)
                .collect(Collectors.toList());

        this.dictionaries = uniqueIndex(ds, d -> d.definition().name());
    }


    public Set<String> getDictionaries() {
        return dictionaries.keySet();
    }

    public Dictionary getDictionary(final String name) {
        return checkNotNull(dictionaries.get(name), "No dictionary found with name %s", name);
    }
}

package net.ethx.tip.data;

import java.util.stream.Stream;

public interface Dictionary {
    Definition definition();

    Stream<String> solve(String in);
}

package net.ethx.tip;

public class Constants {
    public static final int MAX_LETTERS = 16;
    public static final int MAX_UNKNOWN = 2;
}

package net.ethx.tip.data;

import net.ethx.tip.util.PrimeStream;
import org.junit.Test;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class PrimeStreamTest {
    @Test
    public void generate() throws Exception {
        assertEquals(Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29), PrimeStream.generate().limit(10).collect(Collectors.toList()));
    }
}
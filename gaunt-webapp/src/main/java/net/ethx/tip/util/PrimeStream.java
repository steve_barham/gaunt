package net.ethx.tip.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class PrimeStream {
    public static Stream<Integer> generate() {
        return Stream.generate(new Supplier<Integer>() {
            private final List<Integer> primes = new ArrayList<>();
            private int last = 1;

            @Override
            public Integer get() {
                int candidate = last + 1;
                while (true) {
                    boolean valid = true;
                    for (Integer prime : primes) {
                        if (candidate % prime == 0) {
                            valid = false;
                            break;
                        }
                    }

                    if (valid) {
                        primes.add(candidate);
                        last = candidate;
                        return candidate;
                    } else {
                        candidate++;
                    }
                }
            }
        });
    }
}

package net.ethx.tip.util;

import java.util.Iterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.BiFunction;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class MoreStreams {
    public static <T, U, R> Stream<R> zip(Stream<T> ts, Stream<U> us, BiFunction<T, U, R> zipper) {
        Objects.requireNonNull(zipper);

        final Spliterator<T> aSpliterator = Objects.requireNonNull(ts).spliterator();
        final Spliterator<U> bSpliterator = Objects.requireNonNull(us).spliterator();

        final int characteristics = aSpliterator.characteristics() & bSpliterator.characteristics() & ~(Spliterator.DISTINCT | Spliterator.SORTED);

        final long zipSize = ((characteristics & Spliterator.SIZED) != 0)
                ? Math.min(aSpliterator.getExactSizeIfKnown(), bSpliterator.getExactSizeIfKnown())
                : -1;

        final Iterator<T> ti = Spliterators.iterator(aSpliterator);
        final Iterator<U> ui = Spliterators.iterator(bSpliterator);
        final Iterator<R> ri = new Iterator<R>() {
            @Override
            public boolean hasNext() {
                return ti.hasNext() && ui.hasNext();
            }

            @Override
            public R next() {
                return zipper.apply(ti.next(), ui.next());
            }
        };

        Spliterator<R> split = Spliterators.spliterator(ri, zipSize, characteristics);
        return (ts.isParallel() || us.isParallel())
                ? StreamSupport.stream(split, true)
                : StreamSupport.stream(split, false);
    }
}

package net.ethx.tip.util;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.util.stream.Collector;

public class MoreCollectors {
    public static <T> Collector<T, ImmutableSet.Builder<T>, ImmutableSet<T>> immutableSet() {
        return Collector.of(ImmutableSet::builder, (b, t) -> b.add(t), (b1, b2) -> b1.addAll(b2.build()), ImmutableSet.Builder::build);
    }

    public static <T> Collector<T, ImmutableList.Builder<T>, ImmutableList<T>> immutableList() {
        return Collector.of(ImmutableList::builder, (b, t) -> b.add(t), (b1, b2) -> b1.addAll(b2.build()), ImmutableList.Builder::build);
    }
}

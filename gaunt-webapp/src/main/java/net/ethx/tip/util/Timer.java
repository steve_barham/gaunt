package net.ethx.tip.util;

import org.slf4j.Logger;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class Timer {
    public static void time(Runnable runnable, Logger log, TimeUnit timeUnit, String message, Object... args) {
        time(() -> { runnable.run(); return null; }, log, timeUnit, message, args);
    }

    public static <T> T time(Supplier<T> supplier, Logger log, TimeUnit timeUnit, String message, Object... args) {
        final long then = System.nanoTime();
        final T ret = supplier.get();
        final long now = System.nanoTime();

        log.info("{} took {} {}", String.format(message, args), timeUnit.convert(now - then, TimeUnit.NANOSECONDS), timeUnit);
        return ret;
    }
}

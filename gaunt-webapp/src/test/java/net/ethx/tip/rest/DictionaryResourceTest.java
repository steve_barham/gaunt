package net.ethx.tip.rest;

import org.junit.Test;

public class DictionaryResourceTest {
    @Test
    public void checkUnknowns() {
        DictionaryResource.checkUnknowns("??", 2);
        DictionaryResource.checkUnknowns("foo??", 2);
        DictionaryResource.checkUnknowns("?", 2);
        DictionaryResource.checkUnknowns("foo", 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkUnknownsFail() {
        DictionaryResource.checkUnknowns("steve???", 2);
    }

}
package net.ethx.tip.data;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import net.ethx.tip.util.Timer;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkState;
import static net.ethx.tip.util.MoreCollectors.immutableSet;

@JsonAutoDetect(JsonMethod.NONE)
public class Definition {
    private static final Logger log = LoggerFactory.getLogger(Definition.class);

    @JsonProperty("name")
    private final String name;
    @JsonProperty("source")
    private final String source;
    @JsonProperty("scoring")
    private final Map<String, Integer> scoring;

    private final ImmutableMap<Character, Integer> tileScores;
    private final ImmutableSet<String> words;

    @JsonCreator
    public Definition(@JsonProperty("name") String name,
                      @JsonProperty("source") String source,
                      @JsonProperty("scoring") Map<String, Integer> scoring) {
        this.name = name;
        this.source = source;
        this.scoring = ImmutableMap.copyOf(scoring);
        this.words = Timer.time(this::loadWords, log, TimeUnit.MILLISECONDS, "Loading words from %s", source);

        final ImmutableMap.Builder<Character, Integer> tileScores = ImmutableMap.builder();
        for (Map.Entry<String, Integer> entry : scoring.entrySet()) {
            final char[] chars = entry.getKey().toCharArray();
            checkState(chars.length == 1, "Expected all tiles to have length 1, but found '%s'", entry.getKey());
            tileScores.put(chars[0], entry.getValue());
        }
        this.tileScores = tileScores.build();
    }

    public String name() {
        return name;
    }

    public String source() {
        return source;
    }

    public ImmutableSet<Character> tiles() {
        return tileScores.keySet();
    }

    public ImmutableSet<String> words() {
        return words;
    }

    ImmutableSet<String> loadWords() {
        try {
            return Files.readAllLines(Paths.get(Definition.class.getResource(source).toURI())).stream().collect(immutableSet());
        } catch (IOException | URISyntaxException e) {
            throw new IllegalStateException("Could not load " + source, e);
        }
    }

    public static Definition load(String resource) {
        try {
            log.info("Loading {}", resource);
            return new ObjectMapper().readValue(Definition.class.getResource(resource), Definition.class);
        } catch (IOException e) {
            throw new IllegalArgumentException("Could not load " + resource, e);
        }
    }

    @Override
    public String toString() {
        return "Definition{" +
                "name='" + name + '\'' +
                '}';
    }
}
